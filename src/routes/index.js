const express = require('express');
const router = express.Router();

router.get('/', (req, res) =>{
    res.render('index');
})

router.post('/user/signin', (req, res) =>{
res.render('user/signin')
})

router.get('/about', (req, res) =>{
    res.render ('about');
})

module.exports = router;