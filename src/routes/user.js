const express = require('express');
const req = require('express/lib/request');
const router = express.Router();
const passport = require('passport');
const User = require('../models/User');

router.get('/user/signin', (req, res) => {
    res.render('user/signin');
});

router.post('/user/signin', passport.authenticate('local', {
    successRedirect: '/notes',
    failureRedirect: '/user/signin',
    failureFlash: true
}));

router.get('/user/signup', (req, res) => {
    res.render('user/signup');
});

router.post('/user/signup', async (req, res)=> {
    const {username, email, password, confirm_password} = req.body;
    const errors = [];
    console.log(req.body);

    if (username == '') {
        errors.push({text: 'Nombre fatante'});
    }

    if(password != confirm_password){
        errors.push({text:'la contraseña debe coincidir'});
    }
    if (password.length < 4){
        errors.push({text:'La contraseña es muy corta'});
    }
    if(errors.length > 0){
        res.render('user/signup', {errors, username, email, password, confirm_password});
    }
    else{
        const emailUser = await User.findOne({email: email}).lean();
        if(emailUser){
            req.flash('error_msg', 'El email esta en uso');
            res.redirect('/user/signup')
        }
        const newUser = new User({username, email, password});
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
        req.flash('success_msg', 'Estas registrado');
        res.redirect('/user/signin');
    }
});

module.exports = router;