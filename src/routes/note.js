const express = require('express');
const router = express.Router();

const Note = require('../models/Note')

router.get('/note/add', (req, res)=> {
    res.render('notes/new-notes')
});

router.post('/notes/new-notes', async (req, res)=> {
    const {name, code, description, amount, unit_value} = req.body;
    const errors = [];

    if(!name){
        errors.push({text: 'Producto vacio'})
    }
    if(!code){
        errors.push({text: 'Codigo vacio'})
    }
    if(!description){
        errors.push({text: 'Descripcion vacia'})
    }
    if(!amount){
        errors.push({text: 'Cantidad vacia'})
    }
    if(!unit_value){
        errors.push({text: 'valor unitario vacio'})
    }

    if(errors.length>0){
        res.render('notes/new-notes', {
            errors,
            name
        })
    }else{
        const newNote = new Note({name, code, description, amount, unit_value});
        console.log(newNote);
        await newNote.save();
        req.flash('success_msg', 'Producto añadido');
        res.redirect('/notes');
    }

});

router.get('notes/all-notes', async (req, res)=>{
    res.render('notes/new-notes');
})

router.get('/notes', async (req, res) => {
    const notes = await Note.find().sort({date: 'desc'}).lean();
    res.render('notes/all-notes', { notes });
});

router.get('/note/edit/:id', async (req, res)=>{
    const note = await Note.findById(req.params.id).lean();
    res.render('notes/edit-noter', {note});
})

router.put('/notes/edit-noter/:id', async (req, res) =>{
    const {name, code, description, amount, unit_value}= req.body;
    await Note.findByIdAndUpdate(req.params.id, {name, code, description, amount, unit_value}).lean();
    req.flash('success_msg','Producto Actualizado');
    res.redirect('/notes');
})

router.get('/note/bill/:id', async (req, res)=>{
    const note = await Note.findById(req.params.id).lean();
    res.render('notes/bill', {note});
})

router.put('/notes/bill/:id', async (req, res) =>{
    const {name, code, description, amount, unit_value}= req.body;
    await Note.findByIdAndUpdate(req.params.id, {name, code, description, amount, unit_value}).lean();
    req.flash('success_msg','Producto Actualizado');
    res.redirect('/notes');
})

router.delete('/notes/delete/:id', async (req, res)=>{
    await Note.findByIdAndDelete(req.params.id).lean();
    req.flash('error_msg','Producto Eliminado');
    res.redirect('/notes')
})

module.exports = router;