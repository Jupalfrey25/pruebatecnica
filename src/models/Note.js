const mongoose = require('mongoose');
const {Schema} = mongoose;

//name, code, description, amount, unitvalue

const NoteSchema = new Schema({
    name: 
    {type: String, required: true}, 
    code: 
    {type: String, required: true},
    description:
    {type: String, required: true},
    amount:
    {type: Number, required:true},
    unit_value:
    {type: Number, required: true}

});

module.exports = mongoose.model('Note', NoteSchema);